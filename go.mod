module bitbucket.org/uwaploe/tbload

go 1.13

require (
	bitbucket.org/uwaploe/bengalspi v0.2.0
	github.com/BurntSushi/toml v0.3.1
	github.com/briandowns/spinner v1.9.0
)
