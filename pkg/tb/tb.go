// Package tb implements a data structure to hold the inVADER Timing Board
// trigger parameters.
package tb

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// See https://stackoverflow.com/a/54571600
type Duration time.Duration

func (d Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Duration(d).String())
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	switch value := v.(type) {
	case float64:
		*d = Duration(time.Duration(value))
		return nil
	case string:
		tmp, err := time.ParseDuration(value)
		if err != nil {
			return err
		}
		*d = Duration(tmp)
		return nil
	default:
		return errors.New("invalid duration")
	}
}

func (d Duration) MarshalText() ([]byte, error) {
	return []byte(time.Duration(d).String()), nil
}

func (d *Duration) UnmarshalText(text []byte) error {
	val, err := time.ParseDuration(string(text))
	if err != nil {
		return err
	}
	*d = Duration(val)
	return nil
}

// Timing represents the parameters from the inVADER Timing Board FPGA
type Timing struct {
	// Laser diode trigger period
	Dfreq Duration `addr:"0x10,0x11"`
	// Laser diode trigger pulse width
	EDon Duration `addr:"0x12"`
	// Qswitch delay
	QSdelay Duration `addr:"0x13,0x14"`
	// Qswitch pulse width
	QSon Duration `addr:"0x15"`
	// Qswitch + CCD trigger delay
	QSETdelay Duration `addr:"0x16,0x17"`
	// CCD trigger pulse width
	ETon Duration `addr:"0x18"`
	// Qswitch + CCD trigger + CCD gate delay
	QSETEGdelay Duration `addr:"0x19,0x1a"`
	// CCD gate pulse width
	EGon Duration `addr:"0x1b"`
}

const MessageLen = 36

func singleTransaction(addr uint8, val uint16) [3]byte {
	var b [3]byte

	b[2] = addr
	b[1] = uint8(val >> 8)
	b[0] = uint8(val & 0xff)
	return b
}

// MarshalSPI creates a slice of bytes to send to the FPGA via the
// SPI bus. Clkfreq is the clock frequency in Hz and is used to
// convert the durations to clock ticks.
func (tm Timing) MarshalSPI(clkFreq int64) ([]byte, error) {
	var (
		x   uint64
		err error
		j   int
	)

	t := reflect.TypeOf(tm)
	v := reflect.ValueOf(tm)

	buf := make([]byte, 0, MessageLen)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		// Convert value to clock ticks
		ticks := int64(time.Duration(v.Field(i).Interface().(Duration)).Seconds() * float64(clkFreq))
		if tagval, ok := f.Tag.Lookup("addr"); ok {
			addrs := strings.Split(tagval, ",")
			j = 0
			if len(addrs) == 2 {
				// Upper word of a 4-byte value
				x, err = strconv.ParseUint(addrs[j], 0, 8)
				if err != nil {
					return buf, err
				}
				b := singleTransaction(uint8(x), uint16(ticks>>16))
				buf = append(buf, b[:]...)
				j++
			}

			x, err = strconv.ParseUint(addrs[j], 0, 8)
			b := singleTransaction(uint8(x), uint16(ticks))
			buf = append(buf, b[:]...)
		}
	}

	return buf, nil
}
