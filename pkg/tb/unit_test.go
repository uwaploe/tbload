package tb

import (
	"testing"
	"time"
)

func TestMarshal(t *testing.T) {
	table := []struct {
		in  Timing
		clk int64
		out []byte
	}{
		{
			in: Timing{
				Dfreq:       Duration(50 * time.Millisecond),
				EDon:        Duration(30 * time.Microsecond),
				QSdelay:     Duration(200 * time.Microsecond),
				QSon:        Duration(30 * time.Microsecond),
				QSETdelay:   Duration(200*time.Microsecond - 100*time.Nanosecond),
				ETon:        Duration(30 * time.Microsecond),
				QSETEGdelay: Duration(200*time.Microsecond - 100*time.Nanosecond),
				EGon:        Duration(50 * time.Nanosecond),
			},
			clk: 200000000,
			out: []byte{
				0x98, 0x00, 0x10,
				0x80, 0x96, 0x11,
				0x70, 0x17, 0x12,
				0x00, 0x00, 0x13,
				0x40, 0x9c, 0x14,
				0x70, 0x17, 0x15,
				0x00, 0x00, 0x16,
				0x2c, 0x9c, 0x17,
				0x70, 0x17, 0x18,
				0x00, 0x00, 0x19,
				0x2c, 0x9c, 0x1a,
				0x0a, 0x00, 0x1b},
		},
	}

	for _, e := range table {
		b, err := e.in.MarshalSPI(e.clk)
		if err != nil {
			t.Fatal(err)
		}
		if string(b) != string(e.out) {
			t.Errorf("Bad SPI message; expected %q, got %q", e.out, b)
		}
	}
}
