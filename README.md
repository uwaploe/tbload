# Load Timing Board parameters

**This program is deprecated, use <https://bitbucket.org/uwaploe/tbctl/src/master/> instead**

Tbload loads parameters into the inVADER Timing Board FPGA and optionally enables the output.

## Installation

Download the latest Debian Linux packages from the "Downloads" section of this repository and install using the *dpkg* command.

## Usage

``` shellsession
$ tbload --help
Usage: tbload [options] paramfile

Load parameters into the inVADER Timing Board FPGA and optionally enable
the output.
  -debug
    	If true enable debugging output
  -enable
    	If true, enable FPGA output
  -phase string
    	SPI clock phase (rising or falling) (default "rising")
  -pol string
    	SPI clock polarity (low or high) (default "low")
  -sel int
    	SPI slave select (0, 1, 2, or 3)
  -version
    	Show program version information and exit
```

The parameters are stored in a [TOML](https://github.com/toml-lang/toml) format file which is very similar to the Windows "INI" format.

``` toml
# inVADER Timing Board parameters
#
# Clock frequency
Fclk = 200_000_000
[timing]
# Laser diode trigger period
Dfreq = "50ms"
# Laser diode trigger pulse width
EDon = "30us"
# Qswitch delay
QSdelay = "200000ns"
# Qswitch pulse width
QSon = "30us"
# Qswitch + CCD trigger delay
QSETdelay = "199900ns"
# CCD trigger pulse width
ETon = "30us"
# Qswitch + CCD trigger + CCD gate delay
QSETEGdelay = "199900ns"
# CCD gate pulse width
EGon = "50ns"
```

All of the time values consist of a decimal number with a optional fraction and a unit suffix. Valid suffixes are *ns* (nanoseconds), *us* (microseconds), *ms* (milliseconds), and *s* (seconds).

An example parameter file with the default timing values shown above is stored in `/usr/local/share/tbload/timing.toml`, copy it to your own directory and edit as needed.

Use the `--enable` command-line option to enable FPGA output:

``` shellsession
$ tbload --enable timing.toml
Output enabled, type ctrl-c to quit ... /
```

Entering *ctrl-c* disables the output and exits the program.
