// Tbload downloads parameters to the inVADER Timing Board FPGA
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	spi "bitbucket.org/uwaploe/bengalspi"
	"bitbucket.org/uwaploe/tbload/pkg/tb"
	"github.com/BurntSushi/toml"
	"github.com/briandowns/spinner"
)

const Usage = `Usage: tbload [options] paramfile

Load parameters into the inVADER Timing Board FPGA and optionally enable
the output.
`

var Version = "dev"
var BuildDate = "unknown"

var clockPolarity = map[string]spi.ClkPolarity{
	"low":  spi.Low,
	"high": spi.High,
}

var clockPhase = map[string]spi.ClkPhase{
	"rising":  spi.RisingEdge,
	"falling": spi.FallingEdge,
}

var frameLength = map[int]spi.FrameSize{
	8:  spi.Spi8bit,
	16: spi.Spi16bit,
	24: spi.Spi24bit,
	32: spi.Spi32bit,
}

var slaveSelect = map[int]int{
	0: 1,
	1: 2,
	2: 3,
	3: 4,
}

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"If true enable debugging output")
	enableFpga = flag.Bool("enable", false,
		"If true, enable FPGA output")
	slaveSel  int    = 0
	clkPol    string = "low"
	clkPha    string = "rising"
	frameLen  int    = 24
	fpgaClock int64  = 200000000
)

// SPI message to enable FPGA output
const FpgaEnable = "\x01\x00\x00"

// SPI message to disable FPGA output
const FpgaDisable = "\x00\x00\x00"

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func lookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.Atoi(val)
		if err != nil {
			log.Fatalf("LookupEnvOrInt[%s]: %v", key, err)
		}
		return v
	}
	return defaultVal
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&clkPol, "pol",
		lookupEnvOrString("SPI_POLARITY", "low"),
		"SPI clock polarity (low or high)")
	if _, ok := clockPolarity[clkPol]; !ok {
		log.Fatalf("Invalid polarity: %q", clkPol)
	}

	flag.StringVar(&clkPha, "phase",
		lookupEnvOrString("SPI_PHASE", "rising"),
		"SPI clock phase (rising or falling)")
	if _, ok := clockPhase[clkPha]; !ok {
		log.Fatalf("Invalid phase: %q", clkPha)
	}

	flag.IntVar(&slaveSel, "sel",
		lookupEnvOrInt("SPI_SELECT", 0),
		"SPI slave select (0, 1, 2, or 3)")
	if _, ok := slaveSelect[slaveSel]; !ok {
		log.Fatalf("Invalid slave select: %d", slaveSel)
	}

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

type settings struct {
	Fclk int64     `toml:"Fclk"`
	T    tb.Timing `toml:"timing"`
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	contents, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("Cannot read parameter file; %v", err)
	}

	var (
		params settings
		msg    spi.Message
	)

	if _, err := toml.Decode(string(contents), &params); err != nil {
		log.Fatalf("Cannot parse parameters: %v", err)
	}

	if params.Fclk == 0 {
		params.Fclk = fpgaClock
	}

	bus := spi.NewBus(spi.IsaOffset, *debugMode)
	err = bus.Setup(spi.Config{
		Polarity: clockPolarity[clkPol],
		Phase:    clockPhase[clkPha],
		Size:     frameLength[frameLen],
		SlaveSel: slaveSelect[slaveSel],
		Clock:    spi.Freq1Mhz,
		Order:    spi.MsbFirst,
	})
	if err != nil {
		log.Fatalf("Cannot configure SPI interface: %v", err)
	}

	msg.Out = []byte(FpgaDisable)
	_, err = bus.DoXfer(&msg)
	if err != nil {
		log.Fatalf("Cannot disable FPGA output: %v", err)
	}

	msg.Out, err = params.T.MarshalSPI(params.Fclk)
	if err != nil {
		log.Fatalf("Cannot create SPI message: %v", err)
	}

	_, err = bus.DoXfer(&msg)
	if err != nil {
		log.Fatalf("Cannot load timing parameters: %v", err)
	}

	if *enableFpga {
		msg.Out = []byte(FpgaEnable)
		_, err = bus.DoXfer(&msg)
		if err != nil {
			log.Fatalf("Cannot enable FPGA output: %v", err)
		}

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		defer signal.Stop(sigs)

		s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
		s.Prefix = "Output enabled, type ctrl-c to quit ... "
		s.FinalMSG = "\n"
		s.Start()
		<-sigs
		s.Stop()

		msg.Out = []byte(FpgaDisable)
		_, err = bus.DoXfer(&msg)
		if err != nil {
			log.Fatalf("Cannot disable FPGA output: %v", err)
		}
	}
}
